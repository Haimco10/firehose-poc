var AWS = require('aws-sdk');
var express = require('express');
var bodyParser = require('body-parser');
var moment = require('moment');
var HashMap = require('hashmap');
var redis = require("redis");
var CronJob = require('cron').CronJob;
var async = require('async');


// prepate stream details 
var streamMap = new HashMap();
streamMap.set("production","some_production_stream");
streamMap.set("staging","stream_poc");
var KINESIS_REDIS_KEY = "kinesis";
var batchSize = 2;
var cronInterval = '*/5 * * * * *' ;// every minute

// prepare Kinesis Firehose parameters
AWS.config.region = 'us-east-1'
var firehose = new AWS.Firehose();

// prepate redis details 
var client = redis.createClient(6379,'medisafestg.redis.cache.windows.net', {auth_pass: 'JMq5fDQNrFrTkqe8Yl/CN+Bug94TS1uWFOnimyaV1h8=' });


var app = express();
app.use(bodyParser.json()); // support json encoded bodies
app.use(bodyParser.urlencoded({ extended: true })); // support encoded bodies


app.listen(3000, function () {
  console.log('The app  is listening on port 3000!');
})


/*************    send to Kinesis  ******************************************/

// var writeToKinesis = function(data,stream) {

//  var params = {
//   DeliveryStreamName: stream, /* required */
//   Record:{
//     Data: data
//   }
// };
// console.log(data);  
// firehose.putRecord(params, function(err, data) {    
//     if (err) console.log(err); // an error occurred
//           else     console.log(data);           // successful response
//         });
// };


/*************    track API  ******************************************/

app.post('/track', function (req, res) {
  var encData = req.body.data;
  var data = new Buffer(encData,'base64').toString('ascii');
  var jsonArray = JSON.parse(data);
  for( var i = 0; i < jsonArray.length; i++){
    enrichAndSendToKinesis(jsonArray[i],function(err,result){
      if (err) {
        console.log("error:");
        console.log(err);
      }else{
        console.log("result is:");
        console.log(result);
      }

    }); 
  }
  res.send('Got a POST request');
});



/*************    Transform Unit  ***************************************/


var enrichAndSendToKinesis = function(json,callback){

  if (json.hasOwnProperty('date joined')) {
    json["date joined"] = moment(json["date joined"]).format('YYYY-MM-DD HH:mm:ss');
  };
  if (json.hasOwnProperty('timestamp')) {
    json["timestamp"] = moment(json["timestamp"]).format('YYYY-MM-DD HH:mm:ss');
  };


  if(json.hasOwnProperty('environment')) {
    var stream = streamMap.get(json["environment"]);
    var jsonStr = JSON.stringify(json)+'\n';

    client.lpush(KINESIS_REDIS_KEY, jsonStr, function(err, reply) {
      console.log(reply);
    });

    callback(null,"sent successfully");
  }

} 

/*************    Mapping Unit  ******************************************************/



/*************    Cron Job - Sending Batches   ***************************************/

new CronJob(cronInterval, function() {

  console.log('*');

// for (i=0;i<30;i++) {
//   var client = redis.createClient(6379,'medisafestg.redis.cache.windows.net', {auth_pass: 'JMq5fDQNrFrTkqe8Yl/CN+Bug94TS1uWFOnimyaV1h8=' });
//   for (j=0;j<10;j++) {
  
//     client.lpush(KINESIS_REDIS_KEY,"TEST\n",function(err,reply) {});
//     console.log("client:"+i+" event:"+j)
//   }
//     client.quit();
// }
  
  

var records = [];
var isEmptyRedis = false;

async.whilst(
    function () { 
          return !isEmptyRedis;

     },
    function (callback) {
        // do redis request
        client.rpop(KINESIS_REDIS_KEY, function(err, record) {
              if(err){
                  console.log("redis pop error");
                  return callback('err');
              }
                if(record == null){
                    isEmptyRedis = true;
		    if(records.length > 0){
                    	writeBatchToKinesis(records,"stream_poc");	
	            }
                    return callback(null,'empty');
                }

                records.push({Data:record});  

                if(records.length==batchSize){
                    writeBatchToKinesis(records,"stream_poc");
                    records = []
                    return callback(null,'send');
                }   

                 callback(null,'next');     
      });
    },
    function (err, n) {
        if(err){
            console.log(err)
        }else{
          console.log(n);
        }
    }
);
        
}, null, true, 'America/Los_Angeles');



var writeBatchToKinesis = function(data,stream) {

 var params = {
  DeliveryStreamName: stream, /* required */
  Records: data
};
console.log(params);  
firehose.putRecordBatch(params, function(err, data) {    
    if (err) console.log(err); // an error occurred
          else     console.log(data);           // successful response
        });
};

